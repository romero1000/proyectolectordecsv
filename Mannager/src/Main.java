import connection.FileMannager;
import connection.ResultsAccess;
import connection.StatementFile;

import java.util.List;

public class Main {
    public static void main(String []args){
        StatementFile file = new FileMannager(",");
        List<ResultsAccess> readings = file.getDatas("alertas.csv");
        for (ResultsAccess rs : readings){
            System.out.println("ID_ALERTA: "+rs.getInt("id_alerta")+" ID_COHABITANTE: "+rs.getInt("id_cohabitante")+" COLOR: "+ rs.getString("color_alerta"));
        }

        /*
        List<ResultsAccess> readings = file.getDatas("condiciones.csv");
        for (ResultsAccess rs : readings){
            System.out.println("id_condition: "+rs.getInt("id_condicion")+" razon: "+ rs.getString("razon")+" fecha: "+rs.getDate("fecha", "yyyy-MM-dd HH:mm:ss")+" temperatura: "+rs.getDouble("temperatura"));
        }*/
    }
}
