package connection;

import java.util.Date;

public interface ResultsAccess {
    double getDouble(String nameColumn);
    int getInt(String nameColumn);
    String getString(String nameColumn);
    Date getDate(String nameColumn, String formatDate);
}
