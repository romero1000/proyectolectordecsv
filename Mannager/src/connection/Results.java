package connection;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Results implements ResultsAccess{
    private final String [] row;
    private final String [] titlesColumn;
    public Results(String [] titlesColumn, String[] row) {
        this.row=row;
        this.titlesColumn=titlesColumn;
    }
    private boolean existNameColumn(String nameTitleColumn){
        boolean answer = false;
        for(String elm : this.titlesColumn){
            if (elm.equals(nameTitleColumn)){
                answer = true;
                break;
            }
        }
        return answer;
    }
    private int positionTitle(String nameColumn){
        int position=-1;
        for (int i=0; i< this.titlesColumn.length; i++){
            if(this.titlesColumn[i].equals(nameColumn)){
                position = i;
                break;
            }
        }
        return position;
    }

    public static boolean isNumeric(String str) {
        boolean answer;
        try {
            Integer.parseInt(str);
            answer = true;
        } catch (NumberFormatException excepcion) {
            answer = false;
        }
        return answer;
    }
    public static boolean isNumbericDouble(String str) {
        boolean answer;
        try {
            Double.parseDouble(str);
            answer = true;
        } catch (NumberFormatException excepcion) {
            answer = false;
        }
        return answer;
    }

    @Override
    public double getDouble(String nameColumn) {
        double answer = -1;
        if (existNameColumn(nameColumn)){
            int pos = positionTitle(nameColumn);
            if (isNumbericDouble(this.row[pos])){
                answer=Double.parseDouble(this.row[pos]);
            }
        }
        return answer;
    }

    @Override
    public int getInt(String nameColumn) {
        int answer = -1;
        if (existNameColumn(nameColumn)){
            int pos = positionTitle(nameColumn);
            if (isNumeric(this.row[pos])){
                answer=Integer.parseInt(this.row[pos]);
            }
        }
        return answer;
    }

    @Override
    public String getString(String nameColumn) {
        String answer=null;
        if(existNameColumn(nameColumn)){
            int pos = positionTitle(nameColumn);
            answer = this.row[pos];
        }
        return answer;
    }

    @Override
    public Date getDate(String nameColumn, String formatDate) {
        DateFormat dateHour = new SimpleDateFormat(formatDate);
        Date answer=null;

        if (existNameColumn(nameColumn)){
            int pos = positionTitle(nameColumn);
            String date=this.row[pos];
            try {
                answer=dateHour.parse(date);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }

        }
        return answer;
    }
}
