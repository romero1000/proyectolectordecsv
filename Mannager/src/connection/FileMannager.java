package connection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileMannager implements StatementFile{

    private final String separator;

    public FileMannager(String separator){
        this.separator=separator;
    }


    @Override
    public List<ResultsAccess> getDatas(String path) {
        List<ResultsAccess> answer= new ArrayList<>();
        BufferedReader br=null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line;
            String [] titleColumn=null;
            if ((line = br.readLine()) != null) {
                titleColumn = line.split(this.separator);
            }
            while ((line = br.readLine()) != null) {
                String [] row = line.split(this.separator);
                answer.add(new Results(titleColumn,row));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(br!=null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return answer;
    }

    @Override
    public void updateDatas(String headerTitle) {

    }
}
