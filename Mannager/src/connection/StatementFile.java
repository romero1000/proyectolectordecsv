package connection;

import java.util.List;

public interface StatementFile {
    List<ResultsAccess> getDatas(String path);
    void updateDatas(String headerTitle);
}
